import 'package:flutter/material.dart';
 
void main() {
  runApp(MyApp());
}
 
class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}
 
class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text('Food App'),
          ),
          body: ListView(children: <Widget>[
            Center(
                child: Text(
              'Students',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            )),
            DataTable(
              columns: [
                DataColumn(label: Text('ID')),
                DataColumn(label: Text('Menu')),
                DataColumn(label: Text('Price')),
              ],
              rows: [
                DataRow(cells: [
                  DataCell(Text('1')),
                  DataCell(Text('papaya salad')),
                  DataCell(Text('50')),
                ]),
                DataRow(cells: [
                  DataCell(Text('2')),
                  DataCell(Text('sticky rice')),
                  DataCell(Text('20')),
                ]),
                DataRow(cells: [
                  DataCell(Text('3')),
                  DataCell(Text('beefsteak')),
                  DataCell(Text('80')),
                ]),
              ],
            ),
          ])),
    );
  }
}